/* -----------------------------------------------------------------------------------
 Esempio creato partendo dal codice: Sweep di pubblico dominio.
 Muove a sinistra e destra di 180 gradi il servomotore
----------------------------------------------------------------------------------- */
#include <Servo.h>
#define PotPin 0  // Pin analogico collegato al potenziometro
Servo myservo;    // Creo un oggetto Servo per controllare il motore 
  
int valpot;       // variabile utilizzata per leggere il pin del potenziometro
int valmot;       // variabile utilizzata per indicare l'angolo del servo motore

void setup() 
{ 
   Serial.begin(9600);
   myservo.attach(9);  // attacco il Servo al pin 9 (imposto angolo)
} 
  
void loop() 
{ 
   valpot = analogRead(PotPin);            // legge il valore sul potenziometro (tra 0 e 1023) 
   valmot = map(valpot, 0, 1023, 0, 179);  // lo riscala per utilizzarlo come angolo del Servo (valore tra 0 e 179) 
   myservo.write(valmot);                  // setta la posizione del servo 
}
